Cypress.on('uncaught:exception', (err, runnable) => {
// returning false here prevents Cypress from failing the test
return false
})


describe('Open Gigs Site', () => {
it('Gigs Buyer login', () => {
cy.visit('http://dreamguys.co.in/demo/gigs/')
cy.wait(3000) 
cy.get('.navbar-nav > :nth-child(4) > .nav-link').click() // Click login button


//Enter Buyer login credentials
cy.wait(2000) 
cy.get('#user_name').type('Testuser')
cy.get('#password').type('Test@123')    
cy.get('#users_login > .btn-success').click() 



cy.wait(3000) 
cy.get('#gig-services').type('Website design')  // Search the Gig name
cy.get('.top-srch-btn').click()
cy.wait(3000)
cy.get('.ct-dot-1').click()  //Select gig
cy.wait(2000) 
cy.get('p > .btn').click() //Click Buy



//Select payment stripe 
cy.wait(2000)
cy.get(':nth-child(3) > label').click() 

cy.wait(2000)
cy.get('#payment_btn').click()


//Stripe payment details
  cy.get('iframe.stripe_checkout_app').then(function($iframe) {
    const $body = $iframe.contents().find('body')
	cy.wait(2000)
    cy.wrap($body).find('input:eq(0)').type('Test@gmail.com')
	  cy.wait(2000)
	  //card details enter manually
	  cy.wrap($body).contains('Fill in your card details manually.').click()
	  cy.wait(2000)

    cy.wrap($body).find('input:eq(1)').type('4242 4242 4242 4242')
	  cy.wait(2000)
    cy.wrap($body).find('input:eq(2)').type('1221')
	cy.wait(2000)
	cy.wrap($body).find('input:eq(3)').type('123').type('{enter}')
	cy.wait(6000)	
})

// My purchase list  
 cy.get('.thanks-cont > .btn').click()

//Click logout
cy.get('.nav-item.dropdown > .fa').click()
cy.wait(2000)
cy.get('.user-menu-lists > :nth-child(5) > a').click()


})
})


