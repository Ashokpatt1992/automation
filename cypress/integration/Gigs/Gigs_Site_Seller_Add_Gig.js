//import 'cypress-file-upload';

Cypress.on('uncaught:exception', (err, runnable) => {
// returning false here prevents Cypress from failing the test
return false
})


describe('Seller Add New Gig', () => {
it('Gigs Seller login', () => {
cy.visit('http://dreamguys.co.in/demo/gigs/')
cy.wait(3000) 
cy.get('.navbar-nav > :nth-child(4) > .nav-link').click() // Click login button

//--Enter seller login credentials
cy.wait(2000) 
cy.get('#user_name').type('Ashokuser')
cy.get('#password').type('Test@123')
cy.get('#users_login > .btn-success').click()

cy.wait(2000) 
//--Click Sell button
cy.get('.col-sm-7 > :nth-child(2) > :nth-child(1)').click()
cy.wait(2000) 

//===================================
function userID_Alpha_Numeric() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
//================================================
//--Enter Gig details
cy.get('#gig_title').type('Automation Version -'+userID_Alpha_Numeric())
cy.get('#gig_price').type('25')
cy.get('#delivering_time').type('3')


//--Gig category
cy.get('#gig_category_id').select('Automation tool') 

//--Select Buyer options
cy.get(':nth-child(2) > .panel-heading > .panel-title > a > .my-1').click()
cy.wait(2000) 
cy.get('#label_name_1').type('Provide videos and document')
cy.get('#label_val_1').type('5')


//--Enter super fast details
cy.get('#super_fast_delivery').click()
cy.get('#super_fast_delivery_desc').type('Can Deliver videos and document')
cy.get('#super_fast_charges').type('10')

cy.wait(2000)
//-- Select planning work
cy.get('.gigs-form-01 > :nth-child(2) > label').click()


//--Enter Need from buyer
cy.get('#cke_2_contents > .cke_wysiwyg_frame').type('Test@123')



//--Seller image select

cy.wait(2000) 
const Img_filepath= 'SS_01.png'
cy.get(':nth-child(1) > .btn > .fa').click({force: true})
cy.get('#fileopen').click
cy.get('#fileopen').attachFile(Img_filepath)
cy.wait(5000) 
cy.contains('Done').click()
cy.wait(5000) 


//--Seller video select

// const Video_Path='2mb_Video.mp4'
// cy.get(':nth-child(2) > .btn > .fa').click({force: true})
// cy.get('#fileopen').click
// cy.wait(2000) 
// cy.get('#fileopen').attachFile(Video_Path)
// cy.wait(5000) 
// cy.contains('Done').click()


//--Seller post button

 cy.get('#terms_conditions').click()
 cy.get('.post-gigs-btn').click()
cy.screenshot()
cy.wait(2000) 



//Click logout
cy.get('.nav-item.dropdown > .fa').click()
cy.wait(2000)
cy.get('.user-menu-lists > :nth-child(7) > a').click()


})
})